#include "Adafruit_Thermal.h"
#include "SoftwareSerial.h"

#include <EEPROM.h>
#include <PubSubClient.h>
#include <ESP8266WiFi.h>

#define TX_PIN D2
#define RX_PIN D1
#define TRIGGER_PIN D3

#define LINE_WIDTH 32
#define QUILL_DELAY 1000

SoftwareSerial mySerial(RX_PIN, TX_PIN);
Adafruit_Thermal printer(&mySerial);

const char* ssid = "YOUR_WIFI_NETWORK_HERE"; // wifi network
const char* password = "YOUR_WIFI_PASSWORD_HERE"; // wifi password
const char* mqtt_server = "test.mosquitto.org"; // mqtt url
 
WiFiClient espClient;
PubSubClient client(espClient);

void clear(char* line, int start) {
    for (int ndx = start; ndx < LINE_WIDTH + 1; ndx++) {
        line[ndx] = ' ';
    }
    line[LINE_WIDTH] = '\0';
}

void print(char* line) {
  Serial.print("Printing line: '");
  Serial.print(line);
  Serial.print("'...");
  printer.println(line);
  Serial.println("Done");
}

void sendToPrinter(char* message, unsigned int len) {
    Serial.println("Flushing printer");
    printer.flush();
    Serial.print("Printer has paper? ");
    Serial.print(printer.hasPaper());
    Serial.println();
    int msgNdx = -1;
    int lastMsgSpace = -1;
    int lineNdx = 0;
    int lastLineSpace = -1;
   
    char line[LINE_WIDTH + 1];
    clear(line, 0);

    while (++msgNdx < len) {
        line[lineNdx++] = message[msgNdx];
        if (message[msgNdx] == ' ') {
            lastMsgSpace = msgNdx;
            lastLineSpace = lineNdx;
        }
        if (lineNdx >= LINE_WIDTH) {
            if (message[msgNdx] != ' ') {
                if (lastMsgSpace >= 0) {
                    clear(line, lastLineSpace);
                    msgNdx = lastMsgSpace;
                } else {
                    line[lineNdx - 1] = '-';
                    msgNdx--;
                }
            }
            print(line);
            lineNdx = 0;
            clear(line, 0);
            lastMsgSpace = -1;
            lastLineSpace = -1;
        }
    }
    if (lineNdx > 0) {
        print(line);
    }
    Serial.println("Printing blank lines");
    printer.println("\n\n");
}

void callback(char* topic, byte* payload, unsigned int len) {
    Serial.print("Message arrived from topic [");
    Serial.print(topic);
    Serial.println("]");

    Serial.print("Message: '");
    char message[len + 2];
    message[len] = '\n';
    message[len + 1] = '\0';
    for (int i=0; i < len; i++) {
        char receivedChar = (char)payload[i];
        message[i] = receivedChar;
        Serial.print(receivedChar == '\n' ? '*' : receivedChar);
    }
    Serial.println("'");

    Serial.println("Activating quill");
    digitalWrite(BUILTIN_LED, LOW);
    digitalWrite(TRIGGER_PIN, HIGH);

    Serial.println("Waking printer");
    printer.wake();
    delay(QUILL_DELAY);
    sendToPrinter(message, len);
    delay(QUILL_DELAY);
    
    Serial.println("Deactivating quill");
    digitalWrite(TRIGGER_PIN, LOW);
    digitalWrite(BUILTIN_LED, HIGH);
}

void reconnect() {
    while (!client.connected()) {
        Serial.print("Attempting MQTT connection...");
        if (client.connect("make-fun-stuff")) { // you can change this to anything you want
            Serial.println("Connected!");
            client.subscribe("MakeFunStuff"); // add your topic name here
        } else {
            Serial.print("Failed! state=");
            Serial.println(client.state());
            delay(1000);
        }
    }
}

void blink(int count) {
  for (int i = 0; i < count; i++) {
    delay(250);
    digitalWrite(BUILTIN_LED, LOW);
    delay(250);
    digitalWrite(BUILTIN_LED, HIGH);
  }
}

void setup() {
  Serial.begin(9600);

  pinMode(TRIGGER_PIN, OUTPUT);
  digitalWrite(TRIGGER_PIN, LOW);
  pinMode(BUILTIN_LED, OUTPUT);
  digitalWrite(BUILTIN_LED, HIGH);
  
  mySerial.begin(9600);
  printer.begin();

  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.println("Connecting...");
  }
  Serial.print("Connected. IP address: ");
  Serial.println(WiFi.localIP());

  blink(3);

  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
}
 
void loop() {
    if (!client.connected()) {
        reconnect();
    }
    client.loop();
}
